import { Deposit } from "@andre_teros/deposit/";
let testData = [
    {
        month: 1,
        current: 10,
        aim: 70,
    },
    {
        month: 2,
        current: 20,
        aim: 70,
    },
];

const d = new Deposit(testData);
d.add(81);

console.log(d.plan);
